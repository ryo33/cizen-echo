alias Cizen.Effects.{Dispatch, Receive, Start, Subscribe, Monitor}
alias Cizen.{Event, Filter}

defmodule Input do
  defstruct [:value]
end

defmodule Output do
  defstruct [:value]
end

defmodule EchoAutomaton do
  use Cizen.Automaton

  defstruct []

  @impl true
  def spawn(id, _) do
    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %Input{}} -> true end)
    }
    :loop
  end

  @impl true
  def yield(id, :loop) do
    event = perform id, %Receive{}
    %Input{value: value} = event.body
    perform id, %Dispatch{
      body: %Output{value: value}
    }
    :loop
  end
end

defmodule InputAutomaton do
  use Cizen.Automaton

  defstruct []

  @impl true
  def spawn(_, _) do
    :loop
  end

  @impl true
  def yield(id, :loop) do
    case IO.gets "" do
      line when is_binary(line) ->
        perform id, %Dispatch{
          body: %Input{value: line}
        }
        :loop
      _ -> Automaton.finish()
    end
  end
end

defmodule OutputAutomaton do
  use Cizen.Automaton

  defstruct []

  @impl true
  def spawn(id, _) do
    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %Output{}} -> true end)
    }
    :loop
  end

  @impl true
  def yield(id, :loop) do
    event = perform id, %Receive{}
    %Output{value: value} = event.body
    IO.puts(value)
    :loop
  end
end

defmodule Main do
  use Cizen.Effectful

  def main do
    handle fn id ->
      perform id, %Start{
        saga: %EchoAutomaton{}
      }

      input_saga_id = perform id, %Start{
        saga: %InputAutomaton{}
      }

      perform id, %Start{
        saga: %OutputAutomaton{}
      }

      IO.puts("started (try input something)")

      down_filter = perform id, %Monitor{
        saga_id: input_saga_id
      }

      perform id, %Receive{
        event_filter: down_filter
      }
    end
  end
end
Main.main()
